import os
gitbases = []
#add bases
def add_bases(base: str) -> None:
    gitbases.append(base)
    print("added your base "+base)
#import bases from a list
def import_bases() -> None:
    with open('bases.txt', encoding='utf8') as f:
        for line in f:
            add_bases(line.strip())
    print("imported bases")
#add gitclone short
def clone_a_repository_based_on_name_and_branch(repo_name: str, branch_name: str) -> None:
    for base in gitbases:
        dirname = ""
        if branch_name == "main":
            dirname = repo_name
        else:
            dirname = repo_name+"-"+branch_name
        git_clone = "git clone "+base+repo_name+" --branch "+branch_name+" "+dirname
        print(git_clone)
        os.system('cmd /c "{}"'.format(git_clone))
def import_and_clone_based_on_name_and_branch(repo_name: str, branch_name: str) -> None:
    import_bases()
    clone_a_repository_based_on_name_and_branch(repo_name,branch_name)
def clone_dev_branch(repo_name: str) -> None:
    clone_a_repository_based_on_name_and_branch(repo_name,"dev")
def import_and_clone_dev(repo_name: str) -> None:
    import_bases()
    clone_dev_branch(repo_name)
def print_bases() -> None:
    print("printing your bases")
    for base in gitbases:
        print(base)
def cd_and_do(dir_name: str, command: str) -> None:
    os.system('cmd /c "cd {} && {}"'.format(dir_name,command))
def git_pull_from_list(list_file: str) -> None:
    with open(list_file, encoding='utf8') as f:
        for line in f:
            cd_and_do(line.strip(),"git pull")
def start_a_program(name: str) -> None:
    os.system('cmd /c "start {}"'.format(name))
def start_programs_from_list(list: list) -> None:
    for name in list:
        start_a_program(name)
